﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class FakeClick : MonoBehaviour {

	[SerializeField] GameObject[] buttons;

	// Update is called once per frame
	void Update () {
		if(Input.inputString != ""){
			int buttonIndex;
			int.TryParse (Input.inputString, out buttonIndex);
			buttonIndex -= 1;
			if(buttonIndex >=0 && buttonIndex < buttons.Length){
				ExecuteEvents.Execute<IPointerClickHandler>(buttons[buttonIndex],new PointerEventData(EventSystem.current),ExecuteEvents.pointerClickHandler);
			}
		}
	}
}