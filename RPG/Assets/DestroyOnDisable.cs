﻿using UnityEngine;
using System.Collections;

public class DestroyOnDisable : MonoBehaviour {

	// Use this for initialization
	void OnDisable (){
		QuestManger.RaiseEvent (new QuestEvent (QuestEventType.Destroy, gameObject.name));
		Destroy (gameObject);
	}
}
