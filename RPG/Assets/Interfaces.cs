using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public interface IDamagable : IEventSystemHandler {
	void DoDamage (float damage);
}
